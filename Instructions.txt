This is an examination-support artifact, consisting of a demo "office supply store" skeleton
testing framework based on Java, JUnit, WebDriver and RestAssured.

1.  Copy the framework and make operational on your personal computer. Observe how the Web 
    and REST tests work.
    It's a good idea to understand how and why the objects are implemented in the way they 
    are and to consider improvements to this framework.

2.  Business is launching new online services: a new Web store and a REST-like Sales API 
    The following tests are required:

    A.  Web Integration tests - we require a few tests to validate the overall 
	    functionality between different iterations of the database:

		1.  Validate that navigating to http://www.todko.net/ results in the 'qaTest Office 
		    Supplies' website
			
		2.  Add a method to the LogIn page that allows logging in by using one method. Such 
		    as: " .logIn(user, password); "

		3.  Log in with Wrong Credentials and confirm that the user is informed of the error

		4.  Log in with Right Credentials (default/passdef) and confirm that the Store page 
			is returned

		5.  Once logged in, validate that a pencil can be bought: This is a very simple 
		    interaction: A click on the pencil results in a purchase (no payment, shipping 
			or confirmation stages)
			
		6.  Buy the first available Perforator (Left to Right). 
            This interaction has an added step: a confirmation pop up (accept the popup). 
            The same text element as the pencil purchase confirms the perforator purchase
			
		7.  Validate that for each Stamp, there is a confirmation pop up. You don't know in 
		    advance how many Stamps will be offered for sale
			
		8.	Logout and confirm that the user is no longer logged in
			
		9.  Write a utility method that returns the current price, as posted on:
		    http://www.todko.net/printers.html, from a given SKU, use PageObject pattern
		
		10. Write a utility method that returns the current Brand-Model, as posted on:
		    http://www.todko.net/printers.html, of the cheapest available printer

	
	B.	API functional tests - writing initial tests, while API is being developed
	
		API implementation data:
		
			API host:	http://eyqek.mocklab.io
			API root:	sales
			
			Resources:
			
				Resource: 'login'
							Request:
								Method:		 GET
								Headers:  	 Accept=*/*
								Parameters:	 user, pass
							Response (success):
								Code: 		 200
								Headers: 	 Content-Type = application/json;charset=utf-8
								Body:		 <ResponseWithToken>
							Response (fail):
								Code: 		 401
								Headers: 	 Content-Type = application/json;charset=utf-8
								Body:		 <LoginFailed>
							
				Resource: 'buy'
							Request:
								Method:		 POST
								Headers:  	 securitytoken=<ResponseWithToken>.token
								Parameters:	 None
								Body:		 <PrinterOrderForm>
							Response (success):
								Code: 		 200
								Headers: 	 Content-Type = application/xml;charset=utf-8
								Response:	 <Invoice>
							Response (success:
								Code: 		 200
								Headers: 	 Content-Type = application/json;charset=utf-8
								Response:	 <RequirementsUnfullfillable>
							
			Data types and examples:
			
				<ResponseWithToken>
					{
					"token": "tokenAAA",
					"version": "vN.NNNN"
					}
				
				<LoginFailed>
					{
					"message": "Login failed",
					"version": "vN.NNNN"
					}
			
				<PrinterOrderForm>
					{
					"modelyear": NNNN,
					"brand": "AAAAAAAAAAAAAAA",
					"supportedcolors": ["AAAAA", ...]
					}
			
				<Invoice>
					<invoice number="abcNNN">
						<price>NNNNNN</price>
					</invoice>
					
				<RequirementsUnfullfillable>
				{
				"message": "No product with those requirements is available"
				}
			
			
			Existing test data:
			
				user/pass:	admin/passadm
				
				printer available for sale:
				{
				"modelyear": 2021,
				"brand": "HP 123",
				"supportedcolors": ["Cyadn", "Magenta", "Yellow"]
				}
				
				
		1.  Login, using the user/pass from existing test data as GET param's and validate
			that the token is of the right format (the word "token" + a 3-digit number)
		
		2.  Using a valid token, try to buy the printer available for sale and validate
			that the order has gone through

3. 	To submit your answers, please fork our repo to a private repo and create pull requests
    from your private repo. Two pull requests are required: one for enhancing the web tests
	and one for enhancing the REST tests.
			