package uk.interviewoffice.web;

import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import uk.interviewoffice.web.pageobjects.LogInPage;
import uk.interviewoffice.web.pageobjects.StorePage;


@TestMethodOrder(MethodOrderer.MethodName.class)
public class WebFunctionalTest {

    public static WebDriver driver;
    public static LogInPage logInPage;
    public static StorePage store;

    @BeforeAll
    public static void setUp () {
        System.setProperty(WebDriverFramework.CONF_KEY_CHROME, WebDriverFramework.CONF_VALUE_CHROME);

        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(WebDriverFramework.latency, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    public void checkDisplayed(WebElement webElement){
        if (!webElement.isDisplayed()) throw new AssertionError("Element not displayed: " + webElement.getText());
    }

    @AfterAll
    public static void tearDown () {
        driver.manage().deleteAllCookies();
        driver.close();
    }
}
